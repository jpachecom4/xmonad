#!/bin/sh

printf "Installing packages........\n"
sudo xbps-install twm gmp-devel pkg-config libXScrnSaver-devel xz make gcc zlib-devel stack
sudo ln -s /lib/libncurses.so.6.2 /lib/libtinfo.so.6

printf "Setting ~/.local/bin to path........\n"

printf "Setting and installing xmonad with stack......\n"
stack setup
stack update
stack install stack
stack install xmonad
stack install xmonad-contrib
stack install xmobar
